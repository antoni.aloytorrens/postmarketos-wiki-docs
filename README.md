# PostmarketOS Wiki Docs
A script to download pages from PostmarketOS Wiki for offline browsing.

Forked from https://github.com/lahwaacz/arch-wiki-docs .

## Installation

You will need to have the following tools installed:

* python3
* py3-simplemediawiki
* py3-lxml
* py3-cssselect
* py3-requests
* py3-beautifulsoup4

## How to run
<pre>
cd postmarketos-wiki-docs
LANG=en_US.UTF-8 python3         postmarketos-wiki-docs.py         --output-directory "/tmp/build_wiki"         --clean         --safe-filenames && LANG=en_US.UTF-8 python3         add-missing-styles.py         --directory "/tmp/build_wiki/en"
</pre>
The generated wiki will be located at /tmp/build_wiki . You can open it with any browser that supports modern CSS3 and HTML5. JavaScript is not present.