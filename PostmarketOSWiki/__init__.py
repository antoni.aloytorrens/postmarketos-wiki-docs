#! /usr/bin/env python3

__author__ = "Jakub Klinkovský <j.l.k@gmx.com>, Antoni Aloy Torrens <aaloytorrens@gmail.com>"
__url__ = "https://gitlab.com/antoni.aloytorrens/postmarketos-wiki-docs"
__version__ = "0.1"

from .PostmarketOSWiki import *
from .downloader import *
from .optimizer import *
