#! /usr/bin/env python3

import os
import argparse
from bs4 import BeautifulSoup

if __name__ == "__main__":
    aparser = argparse.ArgumentParser(description="Add missing styles for PostmarketOS Wiki")
    aparser.add_argument("--directory", type=str, required=True, help="Path where the HTML files are stored.")
    args = aparser.parse_args()

    # Get the directory
    directory = args.directory

    # for all the files present in that
    # directory
    for filename in os.listdir(directory):
        # check whether the file is having
        # the extension as html and it can
        # be done with endswith function
        if filename.endswith('.html'):
            fname = os.path.join(directory, filename)
            print("Processing ", os.path.abspath(fname))
            # open the file
            file = open(fname)
            soup = BeautifulSoup(file.read(), 'html.parser')
            for table_feature_colors in soup.find_all('table', {'class': 'wikitable feature-colors'}) + soup.find_all('table', {'class': 'cargoTable'}):
                print("Parsing table in ", os.path.abspath(fname))
                # Add style -> border-collapse
                new_table_feature_colors = table_feature_colors
                new_table_feature_colors['style'] = "border-collapse: collapse;"
                columns = new_table_feature_colors.find_all('td')
                for column in columns:
                    # Set color and center text according to status (we'll also need to trim spaces and \n)
                    # Note: .feature and td.feature CSS already aligns to center the text
                    if column.text.replace(" ", "").replace("\n", "") == 'Y':
                        column['class'] = "feature feature-yes"
                    if column.text.replace(" ", "").replace("\n", "") == 'N':
                        column['class'] = "feature"
                    if column.text.replace(" ", "").replace("\n", "") == 'P':
                        column['class'] = "feature feature-partial"
                    if column.text.replace(" ", "").replace("\n", "") == '-':
                        column['class'] = "feature feature-unavailable"
                table_feature_colors.replace_with(new_table_feature_colors)
            # Write to file with newer changes
            with open(fname, "wb") as f_output:
                f_output.write(soup.prettify("utf-8"))
            
            # Row background only in cargoTable
            for table_feature_colors in soup.find_all('table', {'class': 'cargoTable'}):
                print("Parsing rows in ", os.path.abspath(fname))
                new_table_feature_colors = table_feature_colors
                rows = new_table_feature_colors.find_all('tr')
                for idx, row in enumerate(rows):
                        # Every two rows add background
                        if(idx % 2 == 0):
                            row['style'] = "background-color: #eeeeee;"
                table_feature_colors.replace_with(new_table_feature_colors)
            # Write to file with newer changes
            with open(fname, "wb") as f_output:
                f_output.write(soup.prettify("utf-8"))